/* Daniel Selmes 2017. */

#include "geo.hpp"
#include <cmath>
#include <stdlib.h>

/*
Transform
A matrix-like object for representing coordinate transforms, including 2D to 3D
and 3D to 2D projections and extensions.
--------
Members:
	int in: The number of columns on the matrix, the size of the input
		vectors for a transform.
	int out: The number of rows on the matrix, and hence the size of
		the output vetors after a transform.
	double *values: The individual values of the matrix, a linear array
		which is then arranged into a 2D grid based on in and out.

Internally, values is laid out like this:
---
---
---
where the structure is a 2D array which is out rows tall and in cols wide. Thus
an index int rows will be either at values[row][col] ([out][in]) 2D wise or 
values[row*ins + col] ([in*ins + out]) linearly.
*/

/*
Constructor
--------
int in: The number of input values (columns) to the matrix.
int out: The number of output values (rows) to the matrix.
*/
Transform::Transform(int in, int out) : ins(in), outs(out) {
	// Check that in and out are valid.
	if (ins < 1 || outs < 1) {
		throw "Error: Cannot create a matrix of that size.";
	}
	// Sets up values with the default of zero.
	values = (double *)calloc(ins * outs, sizeof(double));
}

/*
Constructor
Creates a new transform based on an array of matrix values.
--------
int in: The number of input values (columns) to the matrix.
int out: The number of output values (rows) to the matrix.
double *vals: The initial values of the matrix in a linear array.
*/
Transform::Transform(int in, int out, double *vals) : ins(in), outs(out) {
        // Check that in and out are valid.
        if (ins < 1 || outs < 1) {
                throw "Error: Cannot create a matrix of that size.";
        }
        // Sets up values with the default of zero.
        values = (double *)calloc(ins * outs, sizeof(double));
	// Uses vals to initialise the matrix:
	for (int col = 0; col < ins; col++) {
		for (int row = 0; row < outs; row++) {
			values[row * ins + col] = vals[row * ins + col];
		}
	}
}

/*
Constructor
Creates a new 2D-2D scale transform based on axis scaling values.
--------
double XScale: The amount of scaling to apply along the X axis
double YScale: The amount of scaling to apply along the Y axis
*/
Transform::Transform(double XScale, double YScale)
{
	this->ins = 2;
	this->outs= 2;
	this->values = (double *)calloc(ins * outs, sizeof(double));
	this->values[0] = XScale;
	this->values[0] = YScale;
}

/*
Constructor
Creates a new 3D-3D scale transform based on axis scaling values.
--------
double XScale: The amount of scaling to apply along the X axis
double YScale: The amount of scaling to apply along the Y axis
double ZScale: The amount of scaling to apply along the Z axis
*/
Transform::Transform(double XScale, double YScale, double ZScale)
{
        ins = 3;
        outs= 3;
        values = (double *)calloc(ins * outs, sizeof(double));
        values[0] = XScale;
        values[4] = YScale;
	values[8] = ZScale;
}

/*
Constructor
Creates a new 2D-2D rotation transform based on a given angle.
--------
double XYRotation: The angle by which to rotate the vector around (counter-
	clockwise on the XY axis). Angle in RADIANS.
*/
Transform::Transform(double XYRotation)
{
	ins = 2;
	outs = 2;
	values = (double *)calloc(ins * outs, sizeof(double));
	values[0] = cos(XYRotation);
	values[1] = -sin(XYRotation);
	values[2] = sin(XYRotation);
	values[3] = cos(XYRotation);
}

/*
extend
Brings a 2D vector into 3D space using this transform.
--------
Vec2 source: The vector to be extended into 3D.
Returns Vec3: The new 3D vector extended from the source.
*/
Vec3 Transform::extend(Vec2 source)
{
	// Make sure this is a 2:3 matrix.
	if (ins != 2 || outs != 3) {
		throw "Cannot extend this vector using this matrix.";
	}
	// Calculate the transform
	Vec3 result(0.0, 0.0, 0.0);
	result.x = source.x * values[0] + source.y * values[1];
	result.y = source.x * values[2] + source.y * values[3];
	result.z = source.x * values[4] + source.y * values[5];
	return result;
}

/*
project
Brings a 3D vector down to 2D space using this transform.
--------
Vec3 source: The vector to be taken into 2D space.
Returns Vec2: The 2D projection of the source vector using the transform.
*/
Vec2 Transform::project(Vec3 source)
{
	// Check to ensure that this is a 3:2 matrix.
	Vec2 result(0.0, 0.0);
	result.x = source.x * values[0] + source.y * values[1] + source.z * values[2];
	result.y = source.x * values[3] + source.y * values[4] + source.z * values[5];
	return result;
}

/*
Multiplication Operator (*) for Transform
Combines two transforms by calculating the product of the transform matrix.
--------
Transform other: The transform we're multiplying with.
Returns Transform: The composition of both transforms.
*/
Transform Transform::operator*(Transform other)
{
	// Check the matrices are multiplicable.
	if (ins != other.outs) {
		throw "Error: Unable to multiply these matrices, incorrect dimensions.\n";
	}
	// Create the resultant transform
	Transform output(other.ins, outs);
	output.ins = other.ins;
	output.outs = outs;
	// Iteratively select rows and columns to dot together
	for (int myrow = 0; myrow < outs; myrow++) {
		for (int othercol = 0; othercol < other.ins; othercol++) {
			// Get the dot product of all the values.
			for (int count = 0; count < ins; count++) {
				output.values[myrow*ins + othercol] = values[myrow*ins + count] * other.values[count*other.ins + othercol];
			}
		}
	}
	return output;
}

/*
Multiplication Operator (*) for Vec2.
Applies a 2D transform to a 2D vector. 
--------
Vec2 vector: The vector to be transformed. 
Returns Vec2: The vector after transformation.
*/
Vec2 Transform::operator*(Vec2 vector)
{
	// Check that this is a 2:2 matrix
	if (ins != 2 || outs != 2) {
		throw "Error: Cannot use this matrix to transform this vector.\n";
	}
	Vec2 result(0.0, 0.0);
	result.x = values[0] * vector.x + values[1] * vector.y;
	result.y = values[2] * vector.x + values[3] * vector.y;
	return result;
}

/*
Multiplication Operator (*) for Vec3
Applies a 3D transformation to a 3D vector.
--------
Vec3 vector: The vector to be transformed.
Returns Vec3: The result of applying this transform.
*/
Vec3 Transform::operator*(Vec3 vector)
{
        // Check that this is a 3:3 matrix
        if (ins != 3 || outs != 3) {
                throw "Error: Cannot use this matrix to transform this vector.\n";
        }
        Vec3 result(0.0, 0.0, 0.0);
        result.x = values[0] * vector.x + values[1] * vector.y + values[2] * vector.z;
        result.y = values[3] * vector.x + values[4] * vector.y + values[5] * vector.z;
        result.z = values[6] * vector.x + values[7] * vector.y + values[8] * vector.z;
        return result;
}


