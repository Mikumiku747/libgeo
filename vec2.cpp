/* Daniel Selmes 2017. */

#include "geo.hpp"
#include <cmath>

/*
Vec2
A vector object based on cartesian vectors. 
--------
Members:
	double x: X component.
	double y: Y component.
*/

/*
Constructor
--------
double xval: X component
double yval: Y component
*/
Vec2::Vec2(double xval, double yval) : x(xval), y(yval) {}

/*
Constructor
--------
*/
Vec2::Vec2() : x(0.0), y(0.0) {}

/*
getMagnitude
Gets the magnitude of the vector (length, or polar coordinate R)
--------
Returns double: The magnitude of this vector.
*/
double Vec2::getMagnitude()
{
	return sqrt(this->x*this->x + this->y*this->y);
}

/*
getAngle
Gets the angle between the positive X axis and this vector's unit vector
direction (or polar unit theta (angle)). Result is in RADIANS!
--------
Returns: This vector's angle.
*/
double Vec2::getAngle()
{
	return atan2(this->y, this->x);
}

/*
cross
Gets the cross product of two vectors (the normal to the plane they both
lie on).
--------
Vec2 other: The vector we're cross multiplying with this one.
Returns Vec3: The cross product of this and the other vector.
*/
Vec3 Vec2::cross(Vec2 other)
{
	Vec3 cross(0.0, 0.0, 0.0);
	cross.z = this->x * other.y - this->y * other.x;
	return cross;
}

/*
unit
Gets the vector of length 1 pointing in the same direction as this vector.
--------
Returns Vec2: This vector's unit vector.
*/
Vec2 Vec2::unit()
{
	return *this / (this->getMagnitude());
}

/*
Addition Operator (+)
Adds two vectors by component.
--------
Vec2 other: The vector being added to this one.
Returns: The sum of both vectors.
*/
Vec2 Vec2::operator+(Vec2 other)
{
	Vec2 sum(0.0, 0.0);
	sum.x = this->x + other.x;
	sum.y = this->y + other.y;
	return sum;
}

/*
Subtraction Operator (-)
Subtracts two vectors by compontent.
--------
Vec2 other: The vector we're subtracting from this one.
Returns Vec2: The differnce between the vectors.
*/
Vec2 Vec2::operator-(Vec2 other)
{
	Vec2 diff(0.0, 0.0);
	diff.x = this->x - other.x;
	diff.y = this->y - other.y;
	return diff;
}
/*
Multiplication Operator (*) for double
Scales this vector by the given scalar value.
--------
double scalar: The amount by which to scale this vector by.
Returns Vec2: The scaled version of this vector.
*/
Vec2 Vec2::operator*(double scalar)
{
	Vec2 scaled(0.0, 0.0);
	scaled.x = this->x*scalar;
	scaled.y = this->y*scalar;
	return scaled;
}

/*
Division Operator (/) for double
Divides this vector's magnitude by the given value (inverse scaling).
--------
double divisor: The value to divide this vector by.
Returns Vec2: The scaled version of this vector.
*/
Vec2 Vec2::operator/(double divisor)
{
	Vec2 divided(0.0, 0.0);
	divided.x = this->x/divisor;
	divided.y = this->y/divisor;
	return divided;
}

/*
Multiplication Operator (*) for Vec2
Multiplies the vectors together to obtain the dot product of the two
vectors.
--------
Vec2 other: The other vector to get the dot product with
Returns double: The dot product of both vectors.
*/
double Vec2::operator*(Vec2 other)
{
	return (this->x * other.x + this->y * other.y);
}
