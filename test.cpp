/*
test
Daniel Selmes 2017
Testing program for the geometry library. 
*/

#include "geo.hpp"
#include <math.h>
#include <iostream>

using namespace std;

int main() {
	// Test 2D Vectors & Transforms
	Vec2 a(1.0, 2.0);
	Vec2 b(2.0, -1.0);
	Transform rot180(M_PI);
	Transform stretch(3.0, 1.0);
	cout << "2D Tests" << endl;
	cout << (a.getMagnitude()) << endl;
	cout << b.getMagnitude() << endl;
	cout << a.getAngle() << endl;
	cout << b.getAngle() << endl;
	cout << (rot180*a).getAngle() << endl;
	cout << (b + (rot180 * b)).x << endl;
	cout << (b + (rot180 * b)).y << endl;
	// Test 3D Vectors
	Vec3 c(1.0, 2.0, 3.0);
	Vec3 d(-3.0, 0.0, 4.0);
	Transform stretched(0.5, 3.0, 2.0);
	double customMatrix[] = {3.0, 2.0, -2.0, 1.0, -3.0, 0.0, 2.0, -1.0, 4.0};
	double identMatrix[] = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
	Transform custom(3, 3, customMatrix);
	Transform ident(3, 3, identMatrix);
	cout << "3D Tests" << endl;
	cout << d.getMagnitude()<< endl;
	cout << (c+d).x << endl;
	cout << (c+d).getXYAngle() << endl;
	cout << (c-d).getMagnitude() << endl;
	cout << (c*-1.0).getMagnitude() << endl;
	cout << (stretched*custom*c).getMagnitude() << endl;
	cout << (custom*stretched*c).getMagnitude() << endl;
	
	return 0;
}
