/*
libgeo
Daniel Selmes 2017
2D and 3D geometry library.
*/

/* Header include protection. */
#ifndef GEO_H
#define GEO_H

/* Vector Classes. */
class Vec2;
class Vec3;
/* Transform Classes. */
class Transform;

class Vec2 {
public:	/* Members. */
	double x;
	double y;
public: /* Member functions. */
	// Constructors
	Vec2 (double xval, double yval);
	Vec2();
	// Member Functions.
	double getMagnitude(); 		// Polar components
	double getAngle();
	Vec3 cross(Vec2 other); 	// Cross Product
	Vec2 unit(); 			// Unit Vector
	// Operator Functions.
	Vec2 operator-(); 		// Negation
	Vec2 operator+(Vec2 other); 	// Vector addition / subtraction.
	Vec2 operator-(Vec2 other);
	Vec2 operator*(double scalar); // Scalar multiplication
	Vec2 operator/(double divisor);
	double operator*(Vec2 other); 	// Vector multiplication (dot product)
};

class Vec3 {
public: /* Members. */
	double x;
	double y;
	double z;
public: /* Member Functions. */
	// Constructors
	Vec3(double xval, double yval, double zval);
	Vec3();
	// Functions
	double getMagnitude();
	double getXYAngle();		// Planar Angles
	double getYZAngle();
	double getXZAngle();
	Vec3 cross(Vec3 other); 	// Cross Product
	Vec3 unit(); 			// Unit Vector
	// Operators
	Vec3 operator+(Vec3 that); 	// Vector Addition / Subtraction
	Vec3 operator-(Vec3 other);
	Vec3 operator*(double scalar);	// Scalar multiplication / division
	Vec3 operator/(double scalar);
	double operator*(Vec3 other); 	// Vector multiplication (dot product)
};

class Transform {
private:/* Members */
	double *values;
	int ins;
	int outs;
public:	/* Member Functions. */
	// Constructors
	Transform(int in, int out);
	Transform(int in, int out, double *vals);
	Transform(double xScale, double yScale);
	Transform(double XScale, double YScale, double ZScale);
	Transform(double XYRotation);
	// Functions
	Vec3 extend(Vec2 source); 		// 2D to 3D
	Vec2 project(Vec3 source);		// 3D to 2D
	// Operators
	Transform operator*(Transform other); 	// Transform Collection
	Vec3 operator*(Vec3 vector); 		// 3D-3D Transforms
	Vec2 operator*(Vec2 vector);		// 2D-2D Transforms
};


#endif
