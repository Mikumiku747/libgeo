/* Daniel Selmes 2017. */

#include "geo.hpp"
#include <cmath>

/*
Vec3
A 3D vector object based on cartesian components.
--------
Members:
	double x: X component.
	double y: Y component.
	double z: Z component.
*/

/*
Constructor
--------
double xval: X component.
double yval: Y component.
double zval: Z component.
*/
Vec3::Vec3(double xval, double yval, double zval) : x(xval), y(yval), z(zval) {}

/* Constructor
--------
*/
Vec3::Vec3() : x(0.0), y(0.0), z(0.0) {}

/*
getMagnitude
Gets the magnitude of the vector (length, polar coordinate R);
--------
Returns double: The lenth of this vector.
*/
double Vec3::getMagnitude()
{
	return sqrt(this->x*this->x + this->y*this->y + this->z*this->z);
}

/*
getXYAngle
Gets the angle the vector makes with the X axis when projected onto the XY 
plane. Note that the angle is in RADIANS.
--------
Returns double: The angle this vector makes from the X axis.
*/
double Vec3::getXYAngle()
{
	return atan2(this->y, this->x);
}

/*
getYZAngle
Gets the angle the vector makes with the Z axis when projected onto the YZ 
plane. Note that the angle is in RADIANS.
--------
Returns double: The angle this vector makes from the Z axis.
*/
double Vec3::getYZAngle()
{
	return atan2(this->y, this->z);
}

/*
getXZAngle
Gets the angle the vector makes with the X axis when projected onto the XZ 
plane. Note that the angle is in RADIANS.
--------
Returns double: The angle this vector makes from the X axis.
*/
double Vec3::getXZAngle()
{
	return atan2(this->z, this->x);
}

/*
cross
Gets the cross product vector of this and another vector.
--------
Vec3 other: The vector to take the cross product of with this one.
Returns Vec3: The cross product of this and the other vector.
*/
Vec3 Vec3::cross(Vec3 other)
{
	Vec3 cross(0.0, 0.0, 0.0);
	cross.x = this->y*other.z - this->z*other.y;
	cross.y = this->z*other.x - this->x*other.z;
	cross.z = this->x*other.y - this->y*other.x;
	return cross;
}

/*
Addition Operator (+)
Gets the sum of this and another Vec3.
--------
Vec3 other: The vector to add to this one.
Returns Vec3: The sum of this and the other vector.
*/
Vec3 Vec3::operator+(Vec3 other)
{
	Vec3 sum(0.0, 0.0, 0.0);
	sum.x = this->x + other.x;
	sum.y = this->y + other.y;
	sum.z = this->z + other.z;
	return sum;
}

/*
Subtraction Operator (+)
Gets the difference of this and another Vec3.
--------
Vec3 other: The vector to subtract from this one.
Returns Vec3: The difference between this and the other vector.
*/
Vec3 Vec3::operator-(Vec3 other)
{
        Vec3 diff(0.0, 0.0, 0.0);
        diff.x = this->x - other.x;
        diff.y = this->y - other.y;
        diff.z = this->z - other.z;
        return diff;
}

/*
Multiplication Operator (*) for double
Scales the vector by the given scalar.
--------
double scalar: The value to scale this vector by.
Returns Vec3: This vector scaled by the scalar. 
*/
Vec3 Vec3::operator*(double scalar)
{
	Vec3 scaled(0.0, 0.0, 0.0);
	scaled.x = this->x * scalar;
	scaled.y = this->y * scalar;
	scaled.z = this->z * scalar;
	return scaled;
}

/*
Division Operator (/) for double
Divides the length of this vector by the given scalar (inverse scaling).
--------
double divisor: The amount to divide the vector by.
Returns Vec3: This vector divided by the given amount.
*/
Vec3 Vec3::operator/(double divisor)
{
	Vec3 divided(0.0, 0.0, 0.0);
	divided.x = this->x / divisor;
	divided.y = this->y / divisor;
	divided.z = this->z / divisor;
	return divided;
}

/*
Multiplication Operator (*) for Vec3
Gets the dot product of this and another vector.
--------
Vec3 other: The vector to get the dot product of along with this one. 
Returns double: The dot product of this and the other vector.
*/
double Vec3::operator*(Vec3 other)
{
	return (this->x * other.x + this->y * other.y + this->z * other.z);
}

