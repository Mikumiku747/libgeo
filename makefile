# makefile
# Daniel Selmes 2017
# makefile for libgeo

# Libraries used
LIBS := 

SOURCES := $(shell find ./ -name "*.cpp" -a -not -name "test.cpp")
OBJS := $(patsubst %.cpp, %.o, $(SOURCES))

CXXFLAGS_OPTS := -Wall -g
#PKGCONFIG_LIBS := $(shell pkg-config --libs --cflags $(LIBS))
CXXFLAGS := $(CXXFLAGS_OPTS) $(PKGCONFIG_LIBS)

all: libgeo.a test
	./test

libgeo.a: $(OBJS)
	$(AR) -cr libgeo.a $(OBJS)

test: test.cpp libgeo.a
	$(CXX) $(CXXFLAGS) -o test test.cpp -L./ -lgeo

.PHONY: clean

clean:
	@rm -rfv *.gch *.o *.a
	@rm -rfv test
